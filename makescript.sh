#!/bin/bash

#TODO
#make distclean only on special command
#support multiple src and build dirs

#the name of the application
binary="YAG"
#directories ('/' at the end always needed)
builddir="./build/"
srcdir="./src"
bindir="./bin/"

#always clean old buildfiles
rm -r "$builddir"
rm -r "$bindir"

#create dirs if not exists
if [ ! -d $builddir ]
then
	mkdir "$builddir"
fi
if [ ! -d $srcdir ]
then
	mkdir "$srcdir"
fi
if [ ! -d $bindir ]
then
	mkdir "$bindir"
fi

#the dirs to inlcude in the linker (at least build dir)
builddirs="$builddir*.o"

##get all subdirectories in src folder to create them in build
#for folder in $(find $srcdir -type d -iname "*" -not -empty -not -path "$srcdir")
#do
#	#remove src dir path to be left with the actual dir name
#	foldername="${folder/$srcdir/}"
#            
#	#add this subdir to be included by the linker
#	builddirs="$builddirs $builddir$foldername/*.o"
#
#	#create src subdir in build
#	if [ ! -d "$builddir$foldername" ]
#	then       
#		echo "creating dir: $foldername"
#		mkdir "$builddir$foldername"
#	fi
#done

for folder in $(find $srcdir -iname "*.cpp" -type f -not -path "$srcdir" -printf '%h\0\n' | sort | uniq ); 
do 
	#remove src dir path to be left with the actual dir name
	foldername="${folder/$srcdir/}";
	echo "folder: $foldername";
	if [ ! -z "$foldername" ]
	then
		#add this subdir to be included by the linker
		builddirs="$builddirs $builddir$foldername/*.o"

		#create src subdir in build
		if [ ! -d "$builddir$foldername" ]
		then       
			echo "creating dir: $foldername"
			mkdir "$builddir$foldername"
		fi
	fi
done

#for every cpp file in src
for file in $(find $srcdir -type f -iname '*.cpp')
do
	#remove src dir path to be left with the actual cpp filename
	cppname="${file/$srcdir/}"

	#echo compile command so the user knows whats being compiled                                                 
	echo g++ -std=c++11 -O3 -g -flto=9 -Wall -Wno-write-strings -c -o "${builddir}${cppname}.o" "${srcdir}/${cppname}"

	#exit here if g++ fails!
	g++ -std=c++11 -O3 -g -flto=9 -Wall -Wno-write-strings -c -o "${builddir}${cppname}.o" "${srcdir}/${cppname}" || exit 1
done

#echo and then use thhe linker on all build dirs
echo g++ -std=c++11 -g -O3 -Wall -Wno-write-strings -Wl,-z,now -rdynamic -fuse-linker-plugin -o "$bindir$binary" $builddirs -lGL -lGLU -lGLEW -lglfw
g++ -std=c++11 -g -O3 -Wall -Wno-write-strings -Wl,-z,now -rdynamic -fuse-linker-plugin -o "$bindir$binary" $builddirs -lGL -lGLU -lGLEW -lglfw
