YAG(YetAnotherGame) - Artificial Neural Networks being animated
=====================================================

Features/Aims:
--------------

- A progressively more complex game to learn a little
- Own simple Game Engine
    
Good to know:
-------------

- the **versioning** system is described here: [semantic versioning](http://semver.org/ "semver.org")
- the **changelog** follows this [guide](https://github.com/olivierlacan/keep-a-changelog "changelog guide")
- for now this work is **licensed** under **ALL RIGHTS RESERVED**
