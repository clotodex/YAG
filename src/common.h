#ifndef __COMMON_H
#define __COMMON_H

//system
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <stdio.h>
#include <chrono>

//opengl
#define GL_GLEXT_PROTOTYPES
#define GLFW_INCLUDE_GLU
#define GLFW_INCLUDE_GLEXT
#include <GL/glew.h>
#include <GLFW/glfw3.h>

//general
//#include "defines.h"
//#include "util/common.h"
//#include "tools.h"
//#include "globals.h"
#include "gltools.h"

//project
#include "glfwbackend.h"
#include "game/common.h"

#endif //__COMMON_H
