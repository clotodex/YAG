#include "common.h"

//the main window
static GLFWwindow* w_main = nullptr;
//the Callback Manager for keyboard and mouse
static GLFWCallbackI* callback;



//terminate GLFW and destroy all windows
void GLFWManager::Terminate()
{
	if(w_main){
		glfwDestroyWindow(w_main);
    }
	w_main = nullptr;
    glfwTerminate();
}

//terminate GLFW and destroy all windows
void GLFWManager::TerminateHard()
{
	Terminate();
	exit(1);
}

//error callback for GLFW
void GLFWManager::GLFWErrorCallback(int error, const char* description)
{
	dprintf(STDOUT_FILENO, "GLFW error (%d): %s\n", error, description);
    TerminateHard();
}


//initialize GLFW
void GLFWManager::Init()
{
    glfwSetErrorCallback(GLFWErrorCallback);

    if (glfwInit() != 1) {
        printf("Error initializing GLFW");
        exit(1);
    }

    int Major, Minor, Rev;

    glfwGetVersion(&Major, &Minor, &Rev);

    printf("GLFW %d.%d.%d initialized\n", Major, Minor, Rev);
}

//pipe keyboard input to callbackmanager
static void KeyboardCallback(GLFWwindow* pWindow, int key, int scancode, int action, int mods)
{
	if(callback)
    callback->KeyboardCallBack(key,action);
	Game::KeyboardCallBack(key,action);
}


//pipe mouse pos to callbackmanager
static void CursorPosCallback(GLFWwindow* pWindow, double x, double y)
{
	if(callback)
    callback->CursorPosCallback(x, y);
	Game::CursorPosCallback(x, y);
}

//pipe mouse input to callbackmanager
static void MouseButtonCallback(GLFWwindow* pWindow, int button, int action, int mode)
{
    double x, y;
    glfwGetCursorPos(pWindow, &x, &y);

	if(callback)
    callback->MouseButtonCallback(button, action, x, y);
	Game::MouseButtonCallback(button, action, x, y);
}

//pipe resize events to callbackmanager
static void ResizeHandler(GLFWwindow* pWindow, GLsizei width, GLsizei height)
{
	if(callback)
    callback->ResizeHandler(width, height);
	Game::ResizeHandler(width, height);
}

//create a glfw window and initialize glew
bool GLFWManager::CreateWindow(int width, int height, bool fullscreen, const char* title, GLFWCallbackI* cb)
{
	if(w_main){
		printf("A window already exists!");
		return false;
	}
	if(!cb){
		printf("No Callback Handler for GLFW specified trying to use Game::!\n");
		//return false;
	}
    callback = cb;

    GLFWmonitor* monitor = fullscreen ? glfwGetPrimaryMonitor() : NULL;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    w_main = glfwCreateWindow(width, height, title, monitor, NULL);

    if (!w_main) {
        printf("error creating window");
        TerminateHard();
    }

	//set window as current gl context
    glfwMakeContextCurrent(w_main);

    // enable glew after glfw
    glewExperimental = GL_TRUE;
    GLenum result = glewInit();
    if (result != GLEW_OK) {
        printf((const char*)glewGetErrorString(result));
        TerminateHard();
    }


    glfwSetKeyCallback(w_main, KeyboardCallback);
    glfwSetCursorPosCallback(w_main, CursorPosCallback);
    glfwSetMouseButtonCallback(w_main, MouseButtonCallback);
    glfwSetWindowSizeCallback(w_main, ResizeHandler);

	ResizeHandler(w_main, width, height);

    return (w_main != nullptr);
}

void GLFWManager::StartLoop()
{
    if (!callback) {
        printf("callback not specified trying to use Game::\n");
        //return;
    }

    glEnable(GL_DEPTH_TEST);

	//GL_FILL for filled, GL_LINE for lines
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    //glFrontFace(GL_CW);
    //glCullFace(GL_BACK);
    //glEnable(GL_CULL_FACE);

    static double lastTime = glfwGetTime();
	  int nbFrames = 0;

    while (!glfwWindowShouldClose(w_main)) {
      // Measure speed
      double currentTime = glfwGetTime();
      nbFrames++;
      if ( currentTime - lastTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
          // printf and reset timer
          printf("%f ms/frame\n", 1000.0/double(nbFrames));
		  printf("FPS: %d\n", nbFrames);
          nbFrames = 0;
          lastTime += 1.0;
      }

		if(callback)
        callback->RenderCallback();
		Game::RenderCallback();
        glfwSwapBuffers(w_main);
        glfwPollEvents();
    }
}

//to exit the loop
void GLFWManager::Stop()
{
    glfwSetWindowShouldClose(w_main, GL_TRUE);
}
