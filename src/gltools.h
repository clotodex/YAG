#ifndef __GLTOOLS_H
#define __GLTOOLS_H

namespace GLTools{
	GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path);
}

#endif //__GLTOOLS_H
