#ifndef __GLFWBACKEND_H
#define __GLFWBACKEND_H

class GLFWCallbackI {
public:
	virtual ~GLFWCallbackI(){};
	virtual void KeyboardCallBack(int key, int action) = 0;
	virtual void CursorPosCallback(double x, double y) = 0;
	virtual void MouseButtonCallback(int button, int action, double x, double y) = 0;
	virtual void ResizeHandler(GLsizei width, GLsizei height) = 0;
	virtual void RenderCallback() = 0;
};

namespace GLFWManager {
	void Terminate();
	void TerminateHard();
	void GLFWErrorCallback(int error, const char* description);
	void Init();
	bool CreateWindow(int Width, int Height, bool fullscreen, const char* title, GLFWCallbackI* callback);
	void StartLoop();
	void Stop();
};
#endif //__GLFWBACKEND_H
