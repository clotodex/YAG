#include "../common.h"

static GLuint VAO;
static GLuint program;

void InitTriangle(){
    const GLfloat vertices[] = {
		0.5f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
	};

	const GLuint indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	glGenVertexArrays(1, &VAO);

    GLuint VBO;
	glGenBuffers(1, &VBO);

    GLuint EBO;
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
	glBindVertexArray(0);

}

void DrawTriangle(){
    glUseProgram(program);

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Game::Init(){
    program = GLTools::LoadShaders("shaders/SimpleVertexShader.vertexshader", "shaders/SimpleFragmentShader.fragmentshader");

	InitTriangle();
}


void Game::KeyboardCallBack(int key, int action)  {};

void Game::CursorPosCallback(double x, double y)  {}

void Game::MouseButtonCallback(int button, int action, double x, double y)  {}

void Game::ResizeHandler(GLsizei width, GLsizei height)
{
	printf("new Size: %dx%d\n", width, height);
	glViewport(0,0, width, height);
}

void Game::RenderCallback()  {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	DrawTriangle();

}
