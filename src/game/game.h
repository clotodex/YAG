#ifndef __GAME__GAME_H
#define __GAME__GAME_H

namespace Game {
		void Init();
		void KeyboardCallBack(int key, int action) ;
		void CursorPosCallback(double x, double y) ;
		void MouseButtonCallback(int button, int action, double x, double y) ;
		void ResizeHandler(GLsizei width, GLsizei height);
		void RenderCallback() ;
};

#endif //__GAME__GAME_H
