#include "common.h"

int main(){
	GLFWManager::Init();

	GLFWManager::CreateWindow(600, 600, false, "YAG", nullptr);

	Game::Init();
	GLFWManager::StartLoop();
	GLFWManager::Terminate();
	return 0;
}
